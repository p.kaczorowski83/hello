<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SINGLE
 * ===============================
 *
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main>
    <?php 
        get_template_part( 'template-parts/partial', 'news-single-header');
        get_template_part( 'template-parts/partial', 'news-single-cnt');
        get_template_part( 'template-parts/partial', 'news-single-other');
    ?>  
    </main>

<?php
get_footer();