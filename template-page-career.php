<?php
/**
 * ===============================
 * TEMPLATE-PAGE-CAREER
 * ===============================
 *
 * Template name: Kariera
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="career" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'career-hero');
            get_template_part( 'template-parts/partial', 'career-team');
            get_template_part( 'template-parts/partial', 'career-community');
            get_template_part( 'template-parts/partial', 'career-boxes');
            get_template_part( 'template-parts/partial', 'career-positions');
        ?>
    </main>

<?php
get_footer();