
/*------------------------------
Register plugins
------------------------------*/
gsap.registerPlugin(ScrollTrigger)

gsap.config({ trialWarn: false });
console.clear();

// ANI
gsap.registerPlugin(ScrollTrigger, SplitText);
const split = new SplitText(".ani", { type: "lines" });

split.lines.forEach((target) => {
  gsap.to(target, {
    backgroundPositionX: 0,
    ease: "none",
    scrollTrigger: {
      trigger: target,
      scrub: 1,
      start: "top center",
      end: "bottom center"
    }
  });
});

// HOME ABOUR IMAGE
if (window.matchMedia("(min-width: 1200px)").matches) {
gsap.set(".home-about-image img", {  scale: 0.7, transformOrigin: "top left"});
gsap.to(".home-about-image img", {
  scale: 1, 
  transformOrigin: "top left",
  duration: 1,
  scrollTrigger: {
    trigger: ".home-about-image",
    start: "top 80%",
    end: "top 30%",
    scrub: 1,
    markers: false,
  }
});
gsap.set(".home-about-image-apla", {autoAlpha: 0,  y: 100});
gsap.to(".home-about-image-apla", {
  autoAlpha: 1,  
  y: 0,
  duration:1,
  scrollTrigger: {
    trigger: ".home-about-image",
    start: "top 15%",
    end: "top -10%",
    scrub: 1,
    markers: false,
  }
});
}



// IMAGE WHAT WE DO
if (window.matchMedia("(min-width: 1200px)").matches) {
let revealContainers = document.querySelectorAll(".what-we-do-boxes li .image");

revealContainers.forEach((container) => {
  let image = container.querySelector("img");
  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: container,
      toggleActions: "restart none none reset"
    }
  });

  tl.set(container, { autoAlpha: 1 });
  tl.from(container, 1.5, {
    xPercent: -100,
    ease: Power2.out
  });
  tl.from(image, 1.5, {
    xPercent: 100,
    scale: 1.3,
    delay: -1.5,
    ease: Power2.out
  });
});

}

// BOXY WHO WE ARE
if (window.matchMedia("(min-width: 1200px)").matches) {
gsap.set(".who-we-are-counter ul li:nth-child(2)", {  y: 100, });
gsap.set(".who-we-are-counter ul li:nth-child(4)", {  y: 100, });
gsap.to(".who-we-are-counter ul li:nth-child(2)", {
  y: 0,
  duration: 1,
  scrollTrigger: {
    trigger: ".who-we-are-counter ul li:nth-child(2)",
    start: "top 90%",
    end: "top 30%",
    scrub: 1,
    markers: false,
  }
});
gsap.to(".who-we-are-counter ul li:nth-child(4)", {
  y: 0,
  duration: 1,
  scrollTrigger: {
    trigger: ".who-we-are-counter ul li:nth-child(2)",
    start: "top 70%",
    end: "top 30%",
    scrub: 1,
    markers: false,
  }
});
}

// WHO WE ARE BELIEVE


if (window.matchMedia("(min-width: 1200px)").matches) {
if (window.matchMedia("(max-width: 1659px)").matches) {
  gsap.to(".who-we-are-believe #image", {
  width: "716px", 
  height: "700px",
  ease: Sine.easeOut, 
  autoRound: false,
  scrollTrigger: {
    trigger: ".who-we-are-believe",
    start: "top +=50px",
    end: "bottom -=0px",
    scrub: 1,
    markers: false,
    pin: true,
  }
});

}

if (window.matchMedia("(min-width: 1660px)").matches) {
  gsap.to(".who-we-are-believe #image", {
  width: "716px", 
  height: "770px",
  ease: Sine.easeOut, 
  autoRound: false,
  scrollTrigger: {
    trigger: ".who-we-are-believe",
    start: "top +=50px",
    end: "bottom -=0px",
    scrub: 1,
    markers: false,
    pin: true,
  }
});
}


gsap.set(".who-we-are-believe-text h4, .who-we-are-believe-text p", {  x: '-300px', opacity: 0 });
gsap.to(".who-we-are-believe-text h4", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".who-we-are-believe-text h4",
    start: "top -=100px",
    end: "top -=500px",
    scrub: 1,
    markers: false,
  }
});
gsap.to(".who-we-are-believe-text p", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".who-we-are-believe-text p",
    start: "top -=100px",
    end: "top -=500px",
    scrub: 1,
    markers: false,
  }
});

}


// MAPS
if (window.matchMedia("(min-width: 1200px)").matches) {
gsap.to(".who-we-are-global-image img", {
  scale: .4,
  scrollTrigger: {
    trigger: ".who-we-are-global-image",
    start: "top 20%",
    end: "bottom",
    scrub: 1,
    markers: false,
    pin: true,
  }
});
}

// TEAM COMMUNITY

if (window.matchMedia("(min-width: 1200px)").matches) {
if (window.matchMedia("(max-width: 1659px)").matches) {
gsap.to(".career-community #image", {
  width: "716px", 
  height: "700px",
  ease: Sine.easeOut, 
  autoRound: false,
  scrollTrigger: {
    trigger: ".career-community",
    start: "top +=50px",
    end: "bottom -=0px",
    scrub: 1,
    markers: false,
    pin: true,
    pinSpacing: true,
  }
});
}
}

if (window.matchMedia("(min-width: 1660px)").matches) {
gsap.to(".career-community #image", {
  width: "716px", 
  height: "770px",
  ease: Sine.easeOut, 
  autoRound: false,
  scrollTrigger: {
    trigger: ".career-community",
    start: "top +=100px",
    end: "bottom -=0px",
    scrub: 1,
    markers: false,
    pin: true,
    pinSpacing: true,
  }
});
}
if (window.matchMedia("(min-width: 1200px)").matches) {
gsap.set(".career-community h4, .career-community p", {  x: '300px', opacity: 0 });
}


if (window.matchMedia("(max-width: 1659px)").matches) {

  gsap.to(".career-community h4", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".career-community h4",
    start: "top -=60%",
    end: "top -=90%",
    scrub: 1,
    markers: false,
  }
});

gsap.to(".career-community p", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".career-community p",
    start: "top -=40%",
    end: "top -=70%",
    scrub: 1.5,
    markers: false,
  }
});
}


if (window.matchMedia("(min-width: 1660px)").matches) {
gsap.to(".career-community h4", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".career-community h4",
    start: "top -=400px",
    end: "top -=800px",
    scrub: 1,
    markers: false,
  }
});
gsap.to(".career-community p", {
  x: '0px', 
  opacity: 1,
  duration: 1,
  scrollTrigger: {
    trigger: ".career-community p",
    start: "top -=200px",
    end: "top -=500px",
    scrub: 1,
    markers: false,
  }
});
}


// CARD

if (window.matchMedia("(min-width: 992px)").matches) {
const cards = gsap.utils.toArray(".career-boxes-card, .home-boxes-card");
const spacer = 0;
const minScale = 1;
const distributor = gsap.utils.distribute({ base: minScale, amount: 0, });

cards.forEach((card, index) => {
  
  const scaleVal = distributor(index, cards[index], cards);
  
  const tween = gsap.to(card, {
    scrollTrigger: {
      trigger: card,
      start: `top top`,
      scrub: false,
      markers: false,
      invalidateOnRefresh: true,
    },
    ease: "none",
    scale: scaleVal
  });

  ScrollTrigger.create({
    trigger: card,
    start: `top-=${index * spacer} top`,
    endTrigger: '.career-boxes, .home-boxes',
    end: `bottom top+=${435 + (cards.length * spacer)}`,
    pin: true,
    pinSpacing: false,
    markers: false,
    id: 'pin',
    invalidateOnRefresh: true,
  });
});
}

// BOXY WORK WHAT WE DO
gsap.set(".what-we-do-work ul li:nth-child(2)", {  y: 100, });
gsap.set(".what-we-do-work ul li:nth-child(3)", {  y: 200, });
gsap.set(".what-we-do-work ul li:nth-child(4)", {  y: 300, });
gsap.to(".what-we-do-work ul li:nth-child(2)", {
  y: 0,
  duration: 1,
  scrollTrigger: {
    trigger: ".what-we-do-work ul li:nth-child(2)",
    start: "top 100%",
    end: "top 40%",
    scrub: 1,
    markers: false,
  }
});
gsap.to(".what-we-do-work ul li:nth-child(3)", {
  y: 0,
  duration: 1,
  scrollTrigger: {
    trigger: ".what-we-do-work ul li:nth-child(3)",
    start: "top 100%",
    end: "top 40%",
    scrub: 1,
    markers: false,
  }
});
gsap.to(".what-we-do-work ul li:nth-child(4)", {
  y: 0,
  duration: 1,
  scrollTrigger: {
    trigger: ".what-we-do-work ul li:nth-child(4)",
    start: "top 100%",
    end: "top 50%",
    scrub: 1,
    markers: false,
  }
});



if (window.matchMedia("(min-width: 1200px)").matches) {

gsap.set("#speed",{});

// apply parallax effect to any element with a data-speed attribute
gsap.to("[data-speed]", {
  y: (i, el) => (1 - parseFloat(el.getAttribute("data-speed"))) * ScrollTrigger.maxScroll(window) ,
  ease: "none",
  scrollTrigger: {
    start: "top",
    end: "bottom",
    scrub: 0,
  }
});
}

if (window.matchMedia("(min-width: 1200px)").matches) {
// TORONOT
gsap.utils.toArray(".contact-toronto, .sustainability-download, .sustainability-esg-bg").forEach(function(container) {
    let image = container.querySelector("img");
    
    let tl = gsap.timeline({
        scrollTrigger: {
          trigger: container,
          scrub: true,
          pin: false,
        },
      }); 
      tl.from(image, {
        scaleY: 1,
        transformOrigin: "50% 50%",
        yPercent: -15,
        ease: "none",
      }).to(image, {
        transformOrigin: "50% 50%",
        scaleY: 1,
        yPercent: 15,
        ease: "none",
      }); 
  });
}

if (window.matchMedia("(min-width: 1200px)").matches) {
  gsap.set(".who-we-are-global-map .city", {  y: 50, opacity: 0 });
gsap.to(".who-we-are-global-map .city", {
   y: 0,
   opacity: 1,
  scrollTrigger: {
    trigger: ".who-we-are-global-cnt",
    start: "top +=300px",
    end: "bottom +=100px",
    scrub: 1,
    markers: false,
  }
});
}


if (window.matchMedia("(min-width: 1200px)").matches) {

gsap.set(".news-single-other .owl-carousel .owl-stage .owl-item:nth-child(1) a", {  y: 120, });
gsap.to(".news-single-other .owl-carousel .owl-stage .owl-item:nth-child(1) a", {
   y: 0,
  scrollTrigger: {
    trigger: ".news-single-other",
    start: "top +=350px",
    end: "bottom bottom",
    scrub: 1,
    markers: false,
  }
});

gsap.set(".news-single-other .owl-carousel .owl-stage .owl-item:nth-child(2) a", {  y: 80, });
gsap.to(".news-single-other .owl-carousel .owl-stage .owl-item:nth-child(2) a", {
   y: 0,
  scrollTrigger: {
    trigger: ".news-single-other",
    start: "top +=350px",
    end: "bottom bottom",
    scrub: 1,
    markers: false,
  }
});

}



if (window.matchMedia("(min-width: 1200px)").matches) {
let sectionContent = document.querySelector('body');
    
    if (sectionContent) {
        Array.from(sectionContent.querySelectorAll(
            '.gsap'
        )).forEach((element) => {
            TweenMax.from(element, 0.8, { scrollTrigger: element, y: '90px', opacity: 0 })
        });        
    }
  }




// HOME CONCACT
// const cursor = document.querySelector(".cursor");
const cursorMedias = document.querySelectorAll(".cursor__media");
const navLinks = document.querySelectorAll(".nav__link");

let activeNavLinkIndex = 0; // Initialize with the first item as active

const tl = gsap.timeline({
  paused: true // Start the timeline paused initially
});

// Set the initial state for the first item (default: first navigation link is active)
cursorMedias[activeNavLinkIndex].classList.add("active");
navLinks[activeNavLinkIndex].classList.add("active"); // Add "active" class to the first nav link

navLinks.forEach((navLink, i) => {
  navLink.addEventListener("mouseover", () => {
    if (i !== activeNavLinkIndex) {
      // Remove the "active" class from all navigation links
      navLinks.forEach((link, index) => {
        if (index !== i) {
          cursorMedias[index].classList.remove("active");
          link.classList.remove("active"); // Remove "active" class from other nav links
          gsap.to(link, {
            color: "", // Restore the original color
            x: 0, // Move back to the original position
            duration: 0.3 // Animation duration
          });
        }
      });

      activeNavLinkIndex = i;
      cursorMedias[i].classList.add("active");

      // Animate the current navLink on mouseover
      gsap.to(navLink, {
        color: "red", // Change the color
        x: 20, // Move 20px to the right
        duration: 0.3 // Animation duration
      });

      tl.play();
    }
  });
});


