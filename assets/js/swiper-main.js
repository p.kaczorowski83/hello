var swiperKariera = new Swiper('.career-gallery-slider .swiper-slider', {
                slidesPerView: "auto",
                spaceBetween: 0,
                speed: 700,
               

                // RWD
                breakpoints: {
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                        loop: true,
                        autoplay: {
                            delay: 2500,
                        },
                    },
                    480: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                        loop: true,
                        autoplay: {
                            delay: 2500,
                        },
                    },
                    920: {
                        autoWidth:true,
                        slidesPerView: 2,
                    }
                },
                grabCursor: true,
            });
    
    // Used for animations on slider dragging

            swiperKariera.on('touchMove', function(){
                $('.swiper-slider').addClass('dragged')
            })
            swiperKariera.on('touchEnd', ({el}) => {
                el.classList.remove('dragged')
            })