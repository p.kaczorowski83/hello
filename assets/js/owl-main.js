$('.news-single-other .owl-carousel').owlCarousel({
    margin: 40,
    loop: false,
    responsive:{
        0:{
            items:1.2,
            margin:20,
            nav: true,
            dots: false,            
        },
        600:{
            items:2,
            margin:20,
            nav: true,
            dots: false, 
        },
        992:{
            items:2.2,
            margin:20,
            nav: true,
            dots: false, 
        },
        1200:{
            items:3
        }
    }
})


$('.home-boxes .owl-carousel').owlCarousel({
   margin: 40,
    loop: false,
    responsive:{
        0:{
            items:1,
            
        },
        600:{
            items:2,
        },
        1200:{
            items:3
        }
    }
})



$('.work-single-other .owl-carousel').owlCarousel({

    loop:true,
    nav:true,
    center: true,
    responsive:{
        0:{
            items:1,
            margin: 40,
        },
        600:{
            items:3,
            margin: 40,
        },
        1200:{
            margin: 40,
            items:3.5
        },
        1660:{
            margin: 60,
            items:3.5
        }
    }
})


$('.what-we-do-work .owl-carousel').owlCarousel({
    loop:true,
    nav:true,
    center: false,
    responsive:{
        0:{
            items:1.2,
            margin: 20,
        },
        600:{
            items:3,
            margin: 40,
        },
        1200:{
            margin: 40,
            items:3.5
        },
        1660:{
            margin: 60,
            items:3.5
        }
    }
})