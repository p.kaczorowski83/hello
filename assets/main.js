(function($) {
    
  init()

    function init () {
    popup()
    filter()
    load()
    hamburger()
    height()
    tabs()
    svg()
    }

    // POPUP
    function popup() {
        // POPUP
        $(document).on('click', '.what-we-do-boxes li .btn-orange', function() {
            $('body').addClass('is-popup');
            $('html').addClass('html-popup');
            $('.popup .popup-cnt-row-image .array').addClass('go');
            var activeTab = $(this).attr('href');
            $(activeTab).addClass('popup-active');
            return false;
        });
        $(document).on('click', '.popup .popup-cnt-row-back .btn-back', function() {
            $('body').removeClass('is-popup');
            $('html').removeClass('html-popup');
            $('.popup').removeClass('popup-active');
            $('.popup .popup-cnt-row-image .array').removeClass('go');
        })
    }

    // WORK
    function filter(form) {
    $('.work-list-cat li').on('click', function() {
        
        $.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        dataType: 'html',
        data: {
        action: 'filter_projects',
        category: $(this).data('slug'),
        },
          beforeSend: function(res) {
             $('.ajax-loader').addClass('is-active');
           },
          success: function(res) {
            $('.ajax-loader').removeClass('is-active');
            $('.work-list-grid').html(res);
          }
        });

    });
}

    // LOAD
    function load() {
        $(window).on("load", function () {
            $('.navbar nav ul li.current-menu-item a').addClass('active');
        });
    }


    // HAMBURGER
    function hamburger() {
        $('.hamburger').on('click', function() {
            $('body, html').toggleClass('menu-open');
            // $('.navbar__fixed').toggleClass('navbar__fixed-mobile');
            $('.mobile-overlay').stop().delay(300).toggleClass('open-m');
            $('.hamburger ').toggleClass('hamburger-active');
        });

        $('.mobile-overlay .menu-mobile li').on("click", function(){
            $('body, html').toggleClass('menu-open');
            // $('.navbar__fixed').toggleClass('navbar__fixed-mobile');
            $('.mobile-overlay').stop().delay(300).removeClass('open-m');
            $('.hamburger').toggleClass('hamburger-active');
            });
    }

    function height() { 
        if (window.matchMedia("(min-width: 992px)").matches) {
        var maxHeight = Math.max.apply(null, $(".contact-branches ul li .address").map(function ()                                                        {
        return $(this).innerHeight();
        }).get());      
        $(".contact-branches ul li .address").height( maxHeight );

        var maxHeightCall = Math.max.apply(null, $(".contact-branches ul li .call").map(function ()                                                        {
        return $(this).innerHeight();
        }).get());      
        $(".contact-branches ul li .call").height( maxHeightCall );

        // var marginHeight = $(".work-list ul li").prev().find('.text').height();
        // var myMargin = marginHeight - marginHeight *2;
        // $(".work-list ul li a").css('margin-top',myMargin);

        $('.work-list ul li').each(function() {
            var prevHeight = $(this).prev().find('.text').height();
            var myMargin = prevHeight - prevHeight *2;
            $(this).find('a').css('margin-top',myMargin);
          });
        }

        if (window.matchMedia("(min-width: 1200px)").matches) {
            $('.what-we-do-work ul li a').each(function() {
            var linkHeight = $(this).find('.text').height();
            var bottomMargin = linkHeight - linkHeight *2 - 40;
            $(this).hover(function() {
                $(this).find('.text').css({'transform': 'translate(' +  (30) +'px, ' + (bottomMargin) + 'px'})
            }, function() {
                $(this).find('.text').css({'transform': 'translate(' +  (0) +'px, ' + (0) + 'px'})
              });
          });
        }

      }

    // TABS 
      function tabs() {
        $('.sustainability-cards-row ul li:first-child').addClass('active');
        $('.sustainability-cards-row .tabs ul li').on("click", function(){
            $('.sustainability-cards-row .tabs ul li').removeClass('active');
            $(this).addClass('active');
            $('.sustainability-cards-row .cards ul li').removeClass('active');
            var activeTab = $(this).find('a').attr('href');
            $(activeTab).addClass('active');
            return false
            });
      } 

    // TABS 
    function svg() {
        // SOCIAL
        $('.sustainability-esg-image svg #Path_53267').on("click", function(){
            $('.sustainability-esg-image svg path').attr('data-info', '');
            $(this).attr('data-info', 'active');
            $('.sustainability-esg-image-cnt').addClass('active');
            $('.sustainability-esg-text .item').removeClass('active');
            setTimeout(function () {
                $('.sustainability-esg-text .item-2').addClass('active');
            }, 300);
            
            return false
        });

        // GOV
        $('.sustainability-esg-image svg #Path_53265').on("click", function(){
            $('.sustainability-esg-image svg path').attr('data-info', '');
             $(this).attr('data-info', 'active');
            $('.sustainability-esg-image-cnt').addClass('active');
            $('.sustainability-esg-text .item').removeClass('active');
            setTimeout(function () {
                $('.sustainability-esg-text .item-3').addClass('active');
            }, 300);
            return false
        });

        // Env
        $('.sustainability-esg-image svg #Path_53266').on("click", function(){
            $('.sustainability-esg-image svg path').attr('data-info', '');
            $(this).attr('data-info', 'active');
            $('.sustainability-esg-image-cnt').addClass('active');
            $('.sustainability-esg-text .item').removeClass('active');
            setTimeout(function () {
                $('.sustainability-esg-text .item-1').addClass('active');
            },300);
            return false
        });

        // BTN
        $('.sustainability-esg-image-cnt .btn-back').on("click", function(){
            $('.sustainability-esg-image-cnt').removeClass('active');
            $('.sustainability-esg-text .item').removeClass('active');
            return false
        });

        $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
            $('.sustainability-esg-image-cnt').removeClass('active');
            $('.sustainability-esg-text .item').removeClass('active');
            }
        });
    }
        
    // ALL

    if ('loading' in HTMLImageElement.prototype) {
        const images = document.querySelectorAll('img[loading="lazy"]');
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        // Dynamically import the LazySizes library
        const script = document.createElement('script');
        script.src =
            'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js';
        document.body.appendChild(script);
    }


    // PLACEHOLDER
    $('input,textarea').focus(function() {
        $(this).data('placeholder', $(this).attr('placeholder'))
        .attr('placeholder', '');
        }).blur(function() {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });

}(jQuery));