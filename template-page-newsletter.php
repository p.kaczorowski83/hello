<?php
/**
 * ===============================
 * TEMPLATE-PAGE-NEWSLETTER
 * ===============================
 *
 * Template name: Newsletter
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="news" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'news-hero');
            get_template_part( 'template-parts/partial', 'news-list');
        ?>
    </main>

<?php
get_footer();