<?php
/**
 * ===============================
 * HOME.PHP - The template for displaying content in the home page
 * Template Name: Home
 * ===============================
 *
 * 
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>
    <main class="home" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'home-hero');
            get_template_part( 'template-parts/partial', 'home-work');
            get_template_part( 'template-parts/partial', 'home-about');
            get_template_part( 'template-parts/partial', 'home-boxes');
            get_template_part( 'template-parts/partial', 'home-news');
            get_template_part( 'template-parts/partial', 'home-contact');
        ?>
    </main>
<?php
get_footer();
