<?php
/**
 * ===============================
 * TEMPLATE-PAGE-WHO-WE-ARE
 * ===============================
 *
 * Template name: Who We Are
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="who-we-are" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'who-we-are-hero');
            get_template_part( 'template-parts/partial', 'who-we-are-lead');
            get_template_part( 'template-parts/partial', 'who-we-are-counter');
            get_template_part( 'template-parts/partial', 'who-we-are-believe');
            get_template_part( 'template-parts/partial', 'who-we-are-global');
            get_template_part( 'template-parts/partial', 'who-we-are-clients');
        ?>
    </main>

<?php
get_footer();