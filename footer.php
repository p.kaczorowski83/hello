<?php
/**
 * ===============================
 * FOOTER.PHP - footer
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<footer id="footer" data-scroll-section="">
    <?php 
        get_template_part( 'template-parts/partial', 'footer-col' );
        get_template_part( 'template-parts/partial', 'footer-links' );
    ?> 
</footer><!-- /#footer -->

<div class="ajax-loader"></div>

<?php 
    get_template_part( 'template-parts/partial', 'popup' );
?>

<?php if (!wp_is_mobile()): ?>
    
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/locomotive-scroll.min.js"></script>
<script>
    (function () {
        const locomotiveScroll = new LocomotiveScroll();
    })();
</script>

<?php endif ?>

</div>

<?php wp_footer(); ?>

<script>

function goBack() {
    window.history.back();
}

AOS.init({
    once: true,
    // disable: 'mobile',
    duration: 1000,
});

</script>

<!-- Accessibility Code for "www.arraymarketing.com" -->
<script>
window.interdeal = {
    "sitekey": "01dd5c1527897757238d2cd4dcfd9759",
    "Position": "Right",
    "Menulang": "EN-CA",
    "domains": {
        "js": "https://cdn.equalweb.com/",
        "acc": "https://access.equalweb.com/"
    },
    "btnStyle": {
        "vPosition": [
            "80%",
            "bottom-left"
        ],
        "scale": [
            "0.5",
            "0.5"
        ],
        "color": {
            "main": "#000000",
            "second": "#ffffff"
        },
        "icon": {
            "type": 11,
            "shape": "circle",
            "outline": true
        }
    }
};
(function(doc, head, body){
    var coreCall             = doc.createElement('script');
    coreCall.src             = 'https://cdn.equalweb.com/core/5.0.6/accessibility.js';
    coreCall.defer           = true;
    coreCall.integrity       = 'sha512-rte1aosyfa9h+YnUZ5NHJoYMmVaAMjoGkGWQgMv5Wy/YBhub9fJsbKgUCVo8RUUL2Mxk2AaezXWDR+LzJUU7Mg==';
    coreCall.crossOrigin     = 'anonymous';
    coreCall.setAttribute('data-cfasync', true );
    body? body.appendChild(coreCall) : head.appendChild(coreCall);
})(document, document.head, document.body);
</script>

</body>
</html>