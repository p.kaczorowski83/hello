<?php
/**
 * ===============================
 * TEMPLATE-PAGE-SUSTAINABILITY
 * ===============================
 *
 * Template name: Sustainability
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="sustainability" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'sustainability-hero');
            get_template_part( 'template-parts/partial', 'sustainability-lead');
            get_template_part( 'template-parts/partial', 'sustainability-esg');
            get_template_part( 'template-parts/partial', 'sustainability-card');
            get_template_part( 'template-parts/partial', 'sustainability-download');
            get_template_part( 'template-parts/partial', 'sustainability-standards');
        ?>
    </main>

<?php
get_footer();