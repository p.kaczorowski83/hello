<?php
/**
 * ===============================
 * TEMPLATE-PAGE-RECRUITER
 * ===============================
 *
 * Template name: Recruiter
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="career" data-scroll-container>
        <?php get_template_part( 'template-parts/partial', 'small-hero'); ?>
       <div class="container container-recruiter">
            <script type='text/javascript' src='https://skk.erecruiter.pl/Code.ashx?cfg=d2da1299be834c07b1c59b866ad61c92'></script>
       </div>
    </main>

<?php
get_footer();