<?php
/**
 * ===============================
 * TEMPLATE-PAGE-CONTACT
 * ===============================
 *
 * Template name: Kontakt
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="contact" data-scroll-container>
        <?php 
            // get_template_part( 'template-parts/partial', 'contact-form');
            get_template_part( 'template-parts/partial', 'contact-toronto');
            get_template_part( 'template-parts/partial', 'contact-branches');
        ?>
    </main>

<?php
get_footer();