<?php
/**
 * ===============================
 * THEME SUPPORT OPTIONS
 * ===============================
 *
 * @package pk
 * @since 1.0.0
 * @version 1.0.0
 */

if ( ! function_exists( 'pk_setup_theme' ) ) :
	function pk_setup_theme() {

		// Make theme available for translation: Translations can be filed in the /languages/ directory
		load_theme_textdomain( 'pk', get_template_directory() . '/languages' );

		// Theme Support
		add_theme_support( 'title-tag' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		) );

		add_filter(
		    'wp_lazy_loading_enabled',
		    function( $result, $tag_name ) {
		        if ( 'img' === $tag_name ) {
		            return false;
		        }
		        return $result;
		    },
		    10,
		    2
		);

		add_theme_support('soil', [
        'clean-up', // Cleaner WordPress markup
        'disable-rest-api', // Disable REST API
        'disable-asset-versioning', // Remove asset versioning
        'disable-trackbacks', // Disable trackbacks
        'js-to-footer', // Move JS to footer
        'nav-walker', // Clean up nav menu markup
        'nice-search', // Redirect /?s=query to /search/query
        'relative-urls', // Convert absolute URLs to relative URLs
    ]);

	}
	add_action( 'after_setup_theme', 'pk_setup_theme' );
endif;

/**
 * Set the max image width.
 */
function pk_max_srcset_image_width() {
	return 2560;
}
add_filter( 'max_srcset_image_width', 'pk_max_srcset_image_width', 10, 2 );

function pk_add_image_sizes() {
	add_image_size( 'image1650', 1650, 930, true );
	add_image_size( 'image1920', 1920, 1060, true );
	add_image_size( 'image1920a', 1920, 0 );
	add_image_size( 'image400', 400, 285, true );
	add_image_size( 'image1240', 1240, 0 );
	add_image_size( 'image530', 1920, 530, true );
	add_image_size( 'image335', 335, 335, true );
	add_image_size( 'image400a', 400, 195, true );
	add_image_size( 'imageWork', 500, 648, true );
	add_image_size( 'image500', 500, 665, true );
	add_image_size( 'image500a', 500, 800, true );
	add_image_size( 'image600', 600, 796, true );
	add_image_size( 'image600', 600, 796, true );
	add_image_size( 'image1000', 1000, 1325, true );
	add_image_size( 'image1920b', 1920, 875, true );
	add_image_size( 'image1920c', 1920, 607, true );
	add_image_size( 'image688', 688, 844, true );
}
add_action( 'init', 'pk_add_image_sizes' );


/**
 * Remove default WP image sizes
 *
 * @param array $sizes Array of media image sizes.
 *
 * @return array
 */
function pk_remove_default_images( $sizes ) {
	unset( $sizes['medium'] ); // 300px
	unset( $sizes['large'] ); // 1024px
	unset( $sizes['medium_large'] ); // 768px
	return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'pk_remove_default_images' );

/**
 * Nav menus
 *
 * @since v1.0
 */
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
			'main-menu'   => 'Main Navigation Menu',
			'footer-menu'   => 'Footer Menu',
		)
	);
}


// Custom Nav Walker: wp_bootstrap4_navwalker()
$custom_walker = get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
if ( is_readable( $custom_walker ) ) {
	require_once $custom_walker;
}

$custom_walker_footer = get_template_directory() . '/inc/wp_bootstrap_navwalker_footer.php';
if ( is_readable( $custom_walker_footer ) ) {
	require_once $custom_walker_footer;
}

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


add_filter( 'script_loader_src', 'wpse47206_src' );
add_filter( 'style_loader_src', 'wpse47206_src' );
function wpse47206_src( $url )
{
    if( is_admin() ) return $url;
    return str_replace( site_url(), '', $url );
}


// MENU 
add_filter( 'nav_menu_css_class', function ( $classes, $item, $args, $depth ) {
    if ( $item->ID == 84 ) {
        if ( is_singular( 'post' ) ) {
            $classes[] = 'current-menu-item';
        }
    } 
    if ( $item->ID == 85 ) {
        if ( is_singular( 'work' ) ) {
            $classes[] = 'current-menu-item';
        }
    } 

    elseif ( $item->ID == 41) {
        if ( is_single( array(280, 275, 274, 273,269, 234, 271, 270, 272))) {
            $classes[] = 'current-menu-item';
        }
    }
    elseif ( $item->ID == 478) {
        if ( is_single( array(325, 425,329,330,332,331,333,335,336))) {
            $classes[] = 'current-menu-item';
        }
    }
    elseif ( $item->ID == 40) {
        if ( is_single( array(276,277,278,279))) {
            $classes[] = 'current-menu-item';
        }
    }
    return $classes;
}, 10, 4 );

add_action( 'template_redirect', function(){
	ob_start( function( $buffer ){
		$buffer = str_replace( array( ' type="text/css"', " type='text/css'" ), '', $buffer );
		$buffer = str_replace( array( ' type="text/javascript"', " type='text/javascript'" ), '', $buffer );        
		return $buffer;
	});
});


function custom_jpeg_quality( $quality, $context ) {
	return 100;
}
add_filter( 'jpeg_quality', 'custom_jpeg_quality', 10, 2 );


// PHONE
function ea_phone_url( $phone_number = false ) {
	$phone_number = str_replace( array( '(', ')', '-', '.', '|', ' ' ), '', $phone_number );
	return esc_url( 'tel:' . $phone_number );
}

function filter_projects() {
  $postType = $_POST['type'];
  $catSlug = $_POST['category'];

  $ajaxposts = new WP_Query([
    'post_type' => 'work',
    'posts_per_page'         => '80',
    'tax_query' => [
      [
         'taxonomy' => 'cat-work',
         'field'    => 'term_id',
         'terms'    => array($catSlug), // example of $termIds = [4,5]
         'operator' => 'IN'
      ],
   ]
  ]);
  $response = '';

  if($ajaxposts->have_posts()) {
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
      $response .= get_template_part('template-parts/partial', 'work-list-loop');
    endwhile;
  } else {
    $response = 'empty';
  }

  echo $response;
  exit;
}
add_action('wp_ajax_filter_projects', 'filter_projects');
add_action('wp_ajax_nopriv_filter_projects', 'filter_projects');


// NEWS FILTER
function filter_akt() {
  $postType = $_POST['type'];
  $catSlug = $_POST['category'];

  $ajaxposts = new WP_Query([
    'post_type' => $postType,
    'posts_per_page' => '80',
    'category_name'  => $catSlug
  ]);
  $response = '';

  if($ajaxposts->have_posts()) {
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
      $response .= get_template_part('template-parts/partial', 'news-list-loop');
    endwhile;
  } else {
    $response = 'empty';
  }

  echo $response;
  exit;
}
add_action('wp_ajax_filter_akt', 'filter_akt');
add_action('wp_ajax_nopriv_filter_akt', 'filter_akt');
