<?php
/**
 * ===============================
 * LOADING FILES - CSS/JS
 * ===============================
 *
 * @package SODOVA
 * @since 1.0.0
 * @version 1.0.0
 */
function pk_scripts_loader() {

	if( !is_admin()){
	      wp_deregister_script('jquery');
	      wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', 
	      false, '1.11.3', true);
	      wp_enqueue_script('jquery');
	 }
	add_action('wp_default_scripts', 'remove_jquery_migrate');

	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array(), time());

	if( is_page( array(15, 25, 10,49,11) ) or is_singular( 'post' ) or is_singular( 'work' ) ) {
	// wp_enqueue_script( 'swiperjs', get_template_directory_uri() . '/assets/js/swiper.js', array( 'jquery' ), time(), 'all');
	// wp_enqueue_script( 'swipermainjs', get_template_directory_uri() . '/assets/js/swiper-main.js', array( 'jquery' ), time(), 'all');
	// wp_enqueue_style( 'swiper', get_template_directory_uri() . '/assets/css/swiper.css', array(), time());
		wp_enqueue_script( 'owljs', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), time(), 'all');
		wp_enqueue_script( 'owl-mainjs', get_template_directory_uri() . '/assets/js/owl-main.js', array( 'jquery' ), time(), 'all');
		wp_enqueue_style( 'owlcss', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), time());
	}

	if( is_page( array(49) )) {
		wp_enqueue_script( 'slider-home', get_template_directory_uri() . '/assets/js/slider-home.js', array( 'jquery' ), time(), 'all');
	}

	// GSAP
	wp_enqueue_script( 'gsapjs', get_template_directory_uri() . '/assets/js/gsap.min.js', array( 'jquery' ), time(), 'all');

    	wp_enqueue_script( 'scrolljs', get_template_directory_uri() . '/assets/js/ScrollTrigger.min.js', array( 'jquery' ), time(), 'all');
    	wp_enqueue_script( 'textjs', get_template_directory_uri() . '/assets/js/SplitText3.min.js', array( 'jquery' ), time(), 'all');
    	wp_enqueue_script( 'gsapanijs', get_template_directory_uri() . '/assets/js/gsap-ani.js', array( 'jquery' ), time(), 'all');

    	// ANIMSITION
    	wp_enqueue_script( 'anijs', get_template_directory_uri() . '/assets/js/animsition.min.js', array( 'jquery' ), time(), 'all');
    	wp_enqueue_script( 'ani-main-js', get_template_directory_uri() . '/assets/js/animsition-main.js', array( 'jquery' ), time(), 'all');
    	wp_enqueue_style( 'ani', get_template_directory_uri() . '/assets/css/animsition.min.css', array(), time());

    	// LOCOMOTIVE SCROLL
    	wp_enqueue_script( 'locojs', get_template_directory_uri() . '/assets/js/locomotive-scroll.min.js', array( 'jquery' ), time(), 'all');
    

	wp_enqueue_script( 'aos', get_template_directory_uri() . '/assets/js/aos.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/assets/js/main.bundle.js', array( 'jquery' ), time(), 'all');
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' );

}
add_action( 'wp_enqueue_scripts', 'pk_scripts_loader' );



