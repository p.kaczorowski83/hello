<?php
/**
 * ===============================
 * LANG STRING - POLYLANG
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

pll_register_string("more", "Read more", "array");
pll_register_string("learn", "Learn more", "array");
pll_register_string("check", "Check all", "array");
pll_register_string("other", "Other news", "array");
pll_register_string("news", "News", "array");
pll_register_string("all", "All projects", "array");
pll_register_string("download", "Download report", "array");