<?php

// Register Custom Post Type
function custom_post_work() {

  $labels = array(
    'name'                  => _x( 'Work', 'Post Type General Name', 'gr' ),
    'singular_name'         => _x( 'Work', 'Post Type Singular Name', 'gr' ),
    'menu_name'             => __( 'Work', 'gr' ),
    'name_admin_bar'        => __( 'Post Type', 'gr' ),
  );
  $agrs = array(
    'label'                 => __( 'Work', 'gr' ),
    'description'           => __( 'Post Type Description', 'gr' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor'),
    'taxonomies'            => array( 'cat-work' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-images-alt2',
  );
  register_post_type( 'work', $agrs );

}
add_action( 'init', 'custom_post_work', 0 );

// Register Custom Taxonomy
function custom_work() {

  $labels = array(
    'name'                       => _x( 'Work', 'Taxonomy General Name', 'gr' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'gr' ),
    'menu_name'                  => __( 'Kategorie', 'gr' ),
    'all_items'                  => __( 'Wszystkie', 'gr' ),
  );
  $agrs = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-work', array( 'work' ), $agrs );  

}
add_action( 'init', 'custom_work', 0 );



function custom_post_retial() {

  $labels = array(
    'name'                  => _x( 'Retail Insights', 'Post Type General Name', 'gr' ),
    'singular_name'         => _x( 'Retail Insights', 'Post Type Singular Name', 'gr' ),
    'menu_name'             => __( 'Retail Insights', 'gr' ),
    'name_admin_bar'        => __( 'Post Type', 'gr' ),
  );
  $agrs = array(
    'label'                 => __( 'Retail Insights', 'gr' ),
    'description'           => __( 'Post Type Description', 'gr' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor'),
    'taxonomies'            => array( 'cat-retial' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-admin-site',
  );
  register_post_type( 'retail-insights', $agrs );

}
add_action( 'init', 'custom_post_retial', 0 );

// Register Custom Taxonomy
function custom_retial() {

  $labels = array(
    'name'                       => _x( 'Retail Insights', 'Taxonomy General Name', 'gr' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'gr' ),
    'menu_name'                  => __( 'Kategorie', 'gr' ),
    'all_items'                  => __( 'Wszystkie', 'gr' ),
  );
  $agrs = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-retial', array( 'retail-insights' ), $agrs );  

}
add_action( 'init', 'custom_retial', 0 );
