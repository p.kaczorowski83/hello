<?php
/**
 * Template Name: Not found
 * Description: Page template 404 Not found
 *
 * @package GR
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();

$title = esc_html( get_field( '404_title','options' ) );
$cnt = get_field( '404_cnt','options' );
$link = get_field( '404_link','options' );
?>

<main class="main page404">
	
	<div class="container">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" class="img-fluid" data-aos="fade-up">
		</a>
		<h1 data-aos="fade-up" class="typo1 typo1-center"><span>404</span><?php echo $title?></h1>
		<p data-aos="fade-up">
			<?php echo $cnt ?>
		</p>
		
		<div class="text-center">	
			<?php if ( $link  ) : ?>
			<div data-aos="fade-up">
				<a href="<?php echo esc_url( $link['url'] ); ?>" class="btn-orange"><?php echo esc_html( $link['title'] ); ?></a>
			</div>
			<?php endif; ?>
		</div>
	</div><!-- end .container -->

</main>



<?php
get_footer();
