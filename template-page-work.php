<?php
/**
 * ===============================
 * TEMPLATE-PAGE-WORK
 * ===============================
 *
 * Template name: Projekty
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="work" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'work-hero');
            get_template_part( 'template-parts/partial', 'work-list');
        ?>
    </main>

<?php
get_footer();