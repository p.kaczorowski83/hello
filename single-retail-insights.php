<?php
/**
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main>
    <?php 
        get_template_part( 'template-parts/partial', 'retail-insights-single-form');
        get_template_part( 'template-parts/partial', 'retail-insights-single-header');
        get_template_part( 'template-parts/partial', 'retail-insights-single-list');
    ?>  
    </main>

<?php
get_footer();