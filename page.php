<?php
/**
 * ===============================
 * PAGE.PHP - The template for displaying default theme page
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
<main class="page-main" data-scroll-container>
	<div class="container">
		<h1 class="typo">
			<?php echo the_title();?>
		</h1>


		<div class="page-main-cnt">
			<?php echo the_content();?>
		</div>
	</div>
</main>

<?php
get_footer();

