<?php
/**
 * ===============================
 * SINGLE RI.PHP - single retail header
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$cnt = get_field('nsl_cnt', 'option');
$slogan = get_field('nsl_slogan', 'option');

?>

<div class="retial-insights-form">
	<div class="container-2">

		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg-contact.png" alt="">

		<h1 class="typo">
			<?php echo $slogan; ?>
		</h1>
		<?php echo $cnt; ?>

		<?php echo do_shortcode('[contact-form-7 id="e204f1e" title="Newsletter"]'); ?>
	</div>
</div>