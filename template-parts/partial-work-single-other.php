<?php
/**
 * ===============================
 * WORK SINGLE OTHER.PHP - work single other
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$work = get_field('work_title_other', 'option');

?>
<div class="work-single-other">

	<div class="container">

	<!-- TITLE -->
	<div class="work-single-other-header">
		<div class="title typo" data-aos="fade-up">
			<?php echo $work; ?>
		</div>
		<div class="button" data-aos="fade-up" data-aos-delay="100">
			<?php $lang = pll_current_language();
			$tr_id = pll_get_post( 15, $lang ); ?>
			<a href="<?php echo the_permalink($tr_id);?>" title="<?php echo pll_e('All projects','array');?>" class="btn-orange"><?php echo _e('All projects','array');?>
			</a>				
		</div>
	</div>
	
	</div>

	<!-- NEWS -->
	<ul class="owl-carousel" data-aos="fade-up">
	<?php
	$query = new WP_Query( array(
		'nopaging'               => false,
		'posts_per_page'         => '13',
		'post_type'              => 'work',
		'post_status'			 => 'publish',
		'post_parent' => $post->post_parent,
          'post__not_in' => array( $wp_query->post->ID ),
	) ); 
	?>
	<?php if ( $query->have_posts() ) : ?>
       	<?php while ( $query->have_posts() ) : $query->the_post();?>
       		<li>
			<a href="<?php the_permalink(); ?>">
				<div class="image">
					<?php $work_img = get_field( 'work_img' ); ?>
					<?php $size = 'image500'; ?>
					<?php if ( $work_img ) : ?>
						<?php echo wp_get_attachment_image( $work_img, $size, false, [
							'class' => 'lazyload img-fluid',
							'loading' => 'lazy',
							'data-src' => wp_get_attachment_image_url( $work_img , $size ),
							'alt' => get_post_meta( $work_img , '_wp_attachment_image_alt', true),
							]); 
						?>
					<?php endif; ?>
				</div>
				
				<div class="text">
					<h4><?php the_title(); ?></h4>
					<?php
					$post_id = get_the_ID(); // Get the current post ID (custom post type)
					$taxonomy_name = 'cat-work'; // Replace with your custom taxonomy slug
					$terms = get_the_terms( $post_id, $taxonomy_name );
					if ( $terms ) {
					$isFirst = true; // Flag to track the first category
					foreach ( $terms as $term ) {
					 $prefix = ($isFirst) ? '' : '. '; // Add a dot and space after the first category
					 echo '<div class="cat">'. $term->name . '</div>';
					 $isFirst = false;
					 }
					}
					?>
				</div>
			</a>
		</li>
       	<?php endwhile;?>
	<?php endif;?>
	<?php wp_reset_postdata(); ?>			  
	</ul>		
</div>	
