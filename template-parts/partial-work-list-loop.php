<li class="gsap">
			<a href="<?php the_permalink(); ?>">
				<div class="image">
					<?php $work_img = get_field( 'work_img' ); ?>
					<?php $size = 'image500'; ?>
					<?php if ( $work_img ) : ?>
						<?php echo wp_get_attachment_image( $work_img, $size, false, [
							'class' => 'lazyload img-fluid',
							'loading' => 'lazy',
							'data-src' => wp_get_attachment_image_url( $work_img , $size ),
							'alt' => get_post_meta( $work_img , '_wp_attachment_image_alt', true),
							]); 
						?>
					<?php endif; ?>
				</div>
				<div class="text">
					<h3><?php the_title(); ?></h3>
					<?php
					$post_id = get_the_ID(); // Get the current post ID (custom post type)
					$taxonomy_name = 'cat-work'; // Replace with your custom taxonomy slug
					$terms = get_the_terms( $post_id, $taxonomy_name );
					if ( $terms ) {
					$isFirst = true; // Flag to track the first category
					foreach ( $terms as $term ) {
					 $prefix = ($isFirst) ? '' : '. '; // Add a dot and space after the first category
					 echo '<div class="cat">'. $term->name . '</div>';
					 $isFirst = false;
					 }
					}
					?>
				</div>
			</a>
		</li>