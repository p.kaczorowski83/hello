<?php
/**
 * ===============================
 * SUB HERO.PHP - sub download
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$sustainability_cards_section = get_field('sustainability_cards_section');
?>
<div class="sustainability-cards" data-scroll-section="">
	<div class="container">
		
		<div class="sustainability-cards-row">

			<div class="tabs" data-aos="fade-up">
				<div class="sticky">
					<h4>
						<?php echo $sustainability_cards_section ?>
					</h4>
					<ul>
					<?php while ( have_rows( 'sustainability_cards' ) ) : the_row();
					$sustainability_cards_title = get_sub_field('sustainability_cards_title') ?>
					<li class="tabs-item">
						<a href="#<?php echo sanitize_title($sustainability_cards_title) ?>">
							<?php echo $sustainability_cards_title ?>
						</a>
					</li>
					<?php endwhile;?>
					</ul>
				</div>
			</div>
			
			<?php if ( have_rows( 'sustainability_cards' ) ) : ?>
			<div class="cards" data-aos="fade-up" data-aos-delay="100">
				<ul>
				<?php while ( have_rows( 'sustainability_cards' ) ) : the_row();
				$sustainability_cards_title = get_sub_field('sustainability_cards_title');
				$sustainability_cards_cnt = get_sub_field('sustainability_cards_cnt');
				?>
				<li id="<?php echo sanitize_title($sustainability_cards_title) ?>">					
					<?php $sustainability_cards_foto = get_sub_field( 'sustainability_cards_foto' ); ?>
					<?php $size = 'image688'; ?>
					<?php if ( $sustainability_cards_foto ) : ?>
						<?php echo wp_get_attachment_image( $sustainability_cards_foto, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $sustainability_cards_foto , $size ),
						'alt' => get_post_meta( $sustainability_cards_foto , '_wp_attachment_image_alt', true),
						]); 
						?>
					<?php endif; ?>

					<div class="apla">
						<div class="title">
							<?php echo $sustainability_cards_title; ?>
						</div>
						<?php echo $sustainability_cards_cnt; ?>
					</div>
				</li>
				<?php endwhile;?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>