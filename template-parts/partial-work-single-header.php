<?php
/**
 * ===============================
 * SINGLE WORK.PHP - single work
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$work_year = get_field('work_year');
$work_country = get_field('work_country');
?>
<div class="work-single full" data-scroll-section="">
	
	<!-- HEADER -->
	<div class="work-single-header" data-scroll="" data-aos="fade-up">
		<h1>
			<?php echo the_title(); ?>
		</h1>

		<div class="info">
			<?php echo $work_year; ?>
			<?php echo $work_country ?>
		</div>
	</div>
