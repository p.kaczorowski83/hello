<?php
/**
 * ===============================
 * HOME BOXES.PHP - boxes section
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="home-boxes" data-scroll-section="">

	<?php if ( have_rows( 'home_boxes' ) ) : ?>
	<ul>		
	<?php $i=0; while ( have_rows( 'home_boxes' ) ) : the_row(); $i++; ?>
	<li class="home-boxes-card" id="card-<?php echo $i;?>">
		<div class="home-boxes-card-row container">

			<div class="text">
				<h4><?php the_sub_field( 'home_boxes_title' ); ?></h4>
				<?php the_sub_field( 'home_boxes_cnt' ); ?>
			</div>

			<!-- FOTO -->
			<?php $home_boxes_img = get_sub_field( 'home_boxes_img' ); ?>
			<?php $size = 'image335'; ?>
			<?php if ( $home_boxes_img ) : ?>
				<div class="image">
					<?php echo wp_get_attachment_image( $home_boxes_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $home_boxes_img , $size ),
						'alt' => get_post_meta( $home_boxes_img , '_wp_attachment_image_alt', true),
						]); 
					?>
				</div>
			<?php endif; ?>
		</div>
	</li>
	<?php endwhile; ?>
	</ul>
	<?php endif; ?>

</div>