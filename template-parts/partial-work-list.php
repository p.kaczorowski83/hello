<?php
/**
 * ===============================
 * WORK.PHP - work list
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$work_title = get_field('work_title');
$work_cnt = get_field('work_cnt')
?>
<div class="work-list mt" data-scroll-section="">

	<!-- HEADER -->
	<div class="work-list-header">
		<div class="container">
			<h1 class="typo" data-aos="fade-up">
				<?php echo $work_title; ?>
			</h1>
		</div>
	</div>

	<div class="container">

		<!-- LEAD, FILTER -->
		<div class="work-list-wrapper">
			
			<div class="lead" data-aos="fade-up">
				<?php echo $work_cnt; ?>
			</div>

			<!-- FILTER -->
			<div class="work-list-cat" data-aos="fade-up">
				<span>
					<?php echo pll_e('filter');?>
				</span>
				<?php 
				$works = get_terms( array(
			        'taxonomy' => 'cat-work',
			        'hide_empty' => true,
			    ) ); ?>
			    <ul>
		            <?php foreach ( $works as $work ) : ?>
		            	<li data-slug="<?php echo $work->term_id ?>" data-type="work">
		            		<?php echo $work->name; ?>
		            	</li>
		            <?php endforeach; ?>
        		</ul>
        	</div>
		</div>
		
		<ul class="work-list-grid">
		<?php
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$ppp = 20;
		$query = new WP_Query( array(
			'nopaging'               => false,
			'posts_per_page' => $ppp,
			'paged' => $paged,
			'post_type'              => 'work',
			'post_status'			 => 'publish',
		) ); 
		?>
		<?php if ( $query->have_posts() ) : ?>
        <?php while ( $query->have_posts() ) : $query->the_post();?>
        	<?php get_template_part( 'template-parts/partial', 'work-list-loop'); ?>
        <?php endwhile;?>
		<?php endif;?>
		<?php wp_reset_postdata(); ?>			  
		</ul>

	</div>
	
</div>