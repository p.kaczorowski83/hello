<?php
/**
 * ===============================
 * WHAT WE DO.PHP - what we do hero
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="what-we-do-boxes" data-scroll-section="">
	<div class="container">
		
	<?php if ( have_rows( 'wwd_boxes' ) ) : ?>
	<ul>
	<?php while ( have_rows( 'wwd_boxes' ) ) : the_row();
	$wwd_boxes_title = get_sub_field( 'wwd_boxes_title' );
	$wwd_boxes_cnt = get_sub_field( 'wwd_boxes_cnt' );
	?>
		<li <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif ?>>
			<?php $wwd_boxes_img = get_sub_field( 'wwd_boxes_img' ); ?>
			<?php $size = 'image500'; ?>
			<?php if ( $wwd_boxes_img ) : ?>
			<div class="image">
				<?php echo wp_get_attachment_image( $wwd_boxes_img, $size, false, [
					'class' => 'lazyload img-fluid',
					'loading' => 'lazy',
					'data-src' => wp_get_attachment_image_url( $wwd_boxes_img , $size ),
					'alt' => get_post_meta( $wwd_boxes_img , '_wp_attachment_image_alt', true),
					]); 
				?>
			</div>
			<?php endif; ?>

			<div class="text">
				<?php if ($wwd_boxes_title): ?>
				<h2 <?php if (!wp_is_mobile()): ?>
					data-aos="fade-left"
				<?php endif ?>>
					<?php echo $wwd_boxes_title ?>
				</h2>
				<?php endif ?>
				<div <?php if (!wp_is_mobile()): ?>
					data-aos="fade-left"
				<?php endif ?>>
					<?php echo $wwd_boxes_cnt; ?>
				</div>

				<div <?php if (!wp_is_mobile()): ?>
					data-aos="fade-left"
				<?php endif ?>>
					<a class="btn-orange" href="#<?php echo sanitize_title($wwd_boxes_title) ?>">
						<?php echo pll_e('Learn more','array');?>
					</a>
				</div>
			</div>
		</li>
	<?php endwhile; ?>
	</ul>
	<?php endif; ?>

	</div>
</div>
