<?php
/**
 * ===============================
 * NEWS SINGLE OTHER.PHP - news single other
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="news-single-other">
	<div class="container">
			
		<div class="news-single-other-header">
			<div class="title" data-aos="fade-up">
				<?php echo pll_e('News','array');?>
			</div>
			<div class="button" data-aos="fade-up" data-aos-delay="100">
				<?php $lang = pll_current_language();
				$tr_id = pll_get_post( 17, $lang ); ?>
				<a href="<?php echo the_permalink($tr_id);?>" title="<?php echo _e('Check all','array');?>"><?php echo _e('Check all','array');?>
				</a>
				
			</div>
		</div>

		<!-- NEWS -->
		<ul class="news-slider owl-carousel" data-aos="fade-up">
		<?php
		$query = new WP_Query( array(
			'nopaging'               => false,
			'posts_per_page'         => '3',
			'post_type'              => 'post',
			'post_status'			 => 'publish',
			'post_parent' => $post->post_parent,
            'post__not_in' => array( $wp_query->post->ID ),
		) ); 
		?>
		<?php if ( $query->have_posts() ) : ?>
        	<?php while ( $query->have_posts() ) : $query->the_post();?>
        		<?php get_template_part( 'template-parts/partial', 'news-list-loop'); ?>
        	<?php endwhile;?>
		<?php endif;?>
		<?php wp_reset_postdata(); ?>			  
		</ul>		

	</div>	
</div>