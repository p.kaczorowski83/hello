<?php
/**
 * ===============================
 * NEWS HERO.PHP - news hero
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$hero_slogan = get_field( 'hero_slogan' );
?>
<div class="hero hero-news" data-scroll-section="">

	<?php $hero_img = get_field( 'hero_img' ); ?>
	<?php $size = 'image1920'; ?>
	<?php if ( $hero_img ) : ?>
	<div class="hero-image">
		<?php echo wp_get_attachment_image( $hero_img, $size, false, [
			'class' => 'lazyload img-fluid',
			'loading' => 'lazy',
			'data-src' => wp_get_attachment_image_url( $hero_img , $size ),
			'alt' => get_post_meta( $hero_img , '_wp_attachment_image_alt', true),
			]); 
		?>
	</div>
	<?php endif; ?>

	<div class="hero-slogan">
		<div class="container">
			<h1 data-scroll="" data-scroll-speed="-.25">
				<?php echo $hero_slogan; ?>
			</h1>
		</div>
	</div>
</div>