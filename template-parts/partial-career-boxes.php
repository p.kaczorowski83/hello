<?php
/**
 * ===============================
 * CAREER BOXES.PHP - boxes section
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="career-boxes" data-scroll-section="">

	<?php if ( have_rows( 'career_boxes' ) ) : ?>
	<ul>		
	<?php $i=0; while ( have_rows( 'career_boxes' ) ) : the_row(); $i++; ?>
	<li class="career-boxes-card" id="card-<?php echo $i;?>">
		<div class="career-boxes-card-row container">

			<div class="text" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>
				<h4><?php the_sub_field( 'career_boxes_title' ); ?></h4>
				<?php the_sub_field( 'career_boxes_cnt' ); ?>
			</div>

			<!-- FOTO -->
			<?php $career_boxes_img = get_sub_field( 'career_boxes_img' ); ?>
			<?php $size = 'image335'; ?>
			<?php if ( $career_boxes_img ) : ?>
				<div class="image" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>
					<?php echo wp_get_attachment_image( $career_boxes_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $career_boxes_img , $size ),
						'alt' => get_post_meta( $career_boxes_img , '_wp_attachment_image_alt', true),
						]); 
					?>	
				</div>
			<?php endif; ?>
		</div>
	</li>
	<?php endwhile; ?>
	</ul>
	<?php endif; ?>

</div>