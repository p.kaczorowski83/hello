<?php
/**
 * ===============================
 * CONTACT BRANCHES.PHP - contact branches
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="contact-branches">	
	<div class="container">

		<!-- LOOP -->
		<?php if ( have_rows( 'contact_branches' ) ) : ?>
		<ul>
			<?php while ( have_rows( 'contact_branches' ) ) : the_row();
			$contact_branches_country = get_sub_field( 'contact_branches_country' );
			$contact_branches_adres = get_sub_field( 'contact_branches_adres' );
			$contact_branches_call = get_sub_field( 'contact_branches_call' );
			?>
				<li data-aos="fade-up">
					<?php if ($contact_branches_country): ?>
						<h3>
							<?php echo $contact_branches_country; ?>
						</h3>
					<?php endif ?>

					<?php if ($contact_branches_adres): ?>
					<div class="address">
						<?php echo $contact_branches_adres; ?>
					</div>
					<?php endif ?>

					<?php if ($contact_branches_call): ?>
					<div class="call">
						<?php echo $contact_branches_call; ?>
					</div>					
					<?php endif ?>
					<?php $contact_branches_map = get_sub_field( 'contact_branches_map' ); ?>
					<?php if ( $contact_branches_map ) : ?>
						<a href="<?php echo esc_url( $contact_branches_map['url'] ); ?>" target="_blank" class="map"><?php echo esc_html( $contact_branches_map['title'] ); ?></a>
					<?php endif; ?>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>
		

	</div>
</div>