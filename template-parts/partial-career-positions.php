<?php
/**
 * ===============================
 * CAREER BOXES.PHP - boxes section
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$career_positions_title = get_field( 'career_positions_title' );
?>
<div class="career-positions" data-scroll-section="">

	<div class="container">
		
		<?php if ($career_positions_title): ?>
		<h5 class="typo" data-aos="fade-up">
			<?php echo $career_positions_title ?>
		</h5>
		<?php endif ?>

		<?php if ( have_rows( 'career_positions_boxes' ) ) : ?>
		<ul data-aos="fade-up">
			<?php while ( have_rows( 'career_positions_boxes' ) ) : the_row();
			$career_positions_boxes_title = get_sub_field( 'career_positions_boxes_title' );
			?>
				<li>
					<?php $career_positions_boxes_link = get_sub_field( 'career_positions_boxes_link' ); ?>
					<?php if ( $career_positions_boxes_link ) : ?>
					<a href="<?php echo esc_url( $career_positions_boxes_link['url'] ); ?>" target="<?php echo esc_attr( $career_positions_boxes_link['target'] ); ?>">
						<?php $career_positions_boxes_img = get_sub_field( 'career_positions_boxes_img' ); ?>
						<?php $size = 'image400a'; ?>
						<?php if ( $career_positions_boxes_img ) : ?>
							<div class="image">
							<?php echo wp_get_attachment_image( $career_positions_boxes_img, $size, false, [
								'class' => 'lazyload img-fluid',
								'loading' => 'lazy',
								'data-src' => wp_get_attachment_image_url( $career_positions_boxes_img , $size ),
								'alt' => get_post_meta( $career_positions_boxes_img , '_wp_attachment_image_alt', true),
								]); 
							?>
							</div>
						<?php endif; ?>
						<?php if ($career_positions_boxes_title): ?>
							<h6>
								<?php echo $career_positions_boxes_title; ?>
							</h6>
						<?php endif ?>
						<span>
							<?php echo esc_html( $career_positions_boxes_link['title'] ); ?>
						</span>
					</a>
					<?php endif; ?>
				</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>

	</div>

</div>