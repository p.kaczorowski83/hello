<?php
/**
 * ===============================
 * SINGLE RI.PHP - single retail header
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="retail-insights-single container" data-scroll-section="">
		
	<!-- HEADER -->
	<div class="retail-insights-single-header" data-scroll="" data-aos="fade-up">
		<h1>
			<?php echo the_title(); ?>
		</h1>
	</div>
