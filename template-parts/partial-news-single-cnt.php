<?php
/**
 * ===============================
 * NEWS SINGLE HEADER.PHP - news single header
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="news-single-image">
	<div class="container" data-aos="fade-up">
		<?php $news_img = get_field( 'news_img' ); ?>
		<?php $size = 'image1240'; ?>
		<?php echo wp_get_attachment_image( $news_img, $size, false, [
			'class' => 'lazyload img-fluid gsap',
			'loading' => 'lazy',
			'data-src' => wp_get_attachment_image_url( $news_img , $size ),
			'alt' => get_post_meta( $news_img , '_wp_attachment_image_alt', true),
			]); 
		?>
	</div>
</div>

<?php if ( have_rows( 'news_cnt' ) ): ?>
<div class="container">
	<div class="news-single-cnt">

		<div class="col">
			<?php while ( have_rows( 'news_cnt' ) ) : the_row(); ?>
				<?php if ( get_row_layout() == 'news_cnt_lead' ) : ?>
					<div class="news-single-cnt-lead" data-aos="fade-up">
						<?php $news_cnt_lead_text = get_sub_field( 'news_cnt_lead_text' );
						echo $news_cnt_lead_text; ?>
					</div>
				<?php elseif ( get_row_layout() == 'news_cnt_text' ) : ?>
					<div class="news-single-cnt-text" data-aos="fade-up">
						<?php $news_cnt_text_item = get_sub_field( 'news_cnt_text_item' );
						echo $news_cnt_text_item;
						?>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>

		<!-- SOCIAL -->
		<div class="col">
			<div class="news-single-cnt-share">
				<ul data-aos="fade-up">
					<li>
						<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo the_permalink(); ?>" target="_blank" title="Udostępnij - LinkedIn"><img width="19" height="19" src="/wp-content/uploads/2024/06/linkedin.svg" alt="" decoding="async">
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>&amp;t=<?php echo the_title();?>" target="_blank" title="Udostępnij - Facebook"><img width="11" height="22" src="https://array.sodova.com.pl/wp-content/uploads/2024/06/facebook-1.svg" alt="" decoding="async"></a>
					</li>
					<li>
						<a href="https://twitter.com/share?url=<?php echo the_permalink(); ?>&amp;via=TWITTER_HANDLE&amp;text=<?php echo the_title();?>" target="_blank" title="Udostępnij - X"><img width="19" height="19" src="/wp-content/uploads/2024/06/twitter-1.svg" alt="" decoding="async"></a>
					</li>
				</ul>
			</div>
		</div>	
	</div>
</div>
<?php endif; ?>