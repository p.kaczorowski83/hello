<?php
/**
 * ===============================
 * POPUP.PHP
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<?php $p=0; while ( have_rows( 'wwd_boxes' ) ) : the_row();
$p++;
$wwd_boxes_popup = get_sub_field('wwd_boxes_popup');
$wwd_boxes_title = get_sub_field( 'wwd_boxes_title' );
?>
<div class="popup" id="<?php echo sanitize_title($wwd_boxes_title) ?>">
    <div class="popup-bg"></div>
    <div class="popup-cnt">
        
        <div class="popup-cnt-row" data-scroll-container>
            
            <!-- BACK -->
            <div class="popup-cnt-row-back">
                <button class="btn-back"></button>
            </div>

            <!-- CNT -->
            <div class="popup-cnt-row-text" data-scroll-section="">
                <div data-scroll="">
                    <?php echo $wwd_boxes_popup ?>
                </div>
            </div>

            <!-- FOTO -->
            <?php $wwd_boxes_img = get_sub_field( 'wwd_boxes_img' ); ?>
            <?php $size = 'image500a'; ?>
            <?php if ( $wwd_boxes_img ) : ?>
            <div class="popup-cnt-row-image" data-scroll="">
                <?php echo wp_get_attachment_image( $wwd_boxes_img, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $wwd_boxes_img , $size ),
                    'alt' => get_post_meta( $wwd_boxes_img , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
                <!-- <div class="array">
                    <img src="https://array.sodova.com.pl/wp-content/uploads/2024/07/array.png" class="img-fluid">
                </div> -->
            </div>
            <?php endif; ?>
        </div>

    </div>
</div>
<?php endwhile;?>