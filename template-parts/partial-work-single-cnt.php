<?php
/**
 * ===============================
 * SINGLE WORK.PHP - single work cnt
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$work_cnt = get_field('work_cnt');
?>
<div class="work-single-cnt" data-scroll-section="">

	<div class="text" data-aos="fade-up">
		<div class="sticky">
			<?php echo $work_cnt; ?>
		</div>
	</div>

	<div class="gallery" data-aos="fade-up" data-aos-delay="100">

		<?php $images = get_field('work_gallery'); ?>
		<?php if ( $images ) :  ?>
			<ul>
				<?php foreach( $images as $image ):
				$data_type = pathinfo($image['url'], PATHINFO_EXTENSION);
				if ($data_type == 'mp4') {?>
				    <li>
				    	<video controls>
				      		<source src="<?php echo $image['url'];?>" type="video/mp4">
				    	</video>
					</li>
				  <?php } else { ?>
				    <li data-aos="fade-up">
						<img src="<?php echo esc_url( $image['sizes']['image1240'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" class="img-fluid" />
					</li>
				  <?php } endforeach; 
				?>
				
			</ul>
		<?php endif;?>
	</div>

</div>



</div><!-- edn .work-single -->