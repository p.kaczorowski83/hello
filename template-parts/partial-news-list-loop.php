<?php
/**
 * ===============================
 * NEWS LIST LOOP.PHP - news list loop
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<li>
	<a href="<?php echo the_permalink();?>">
		<div class="image">
			<?php $news_img = get_field( 'news_img' ); ?>
			<?php $size = 'image400'; ?>
			<?php if ( $news_img ) : ?>
				<?php echo wp_get_attachment_image( $news_img, $size, false, [
				'class' => 'lazyload img-fluid',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $news_img , $size ),
				'alt' => get_post_meta( $news_img , '_wp_attachment_image_alt', true),
				]); 
				?>
			<?php endif; ?>
			<span class="date">
				<?php echo the_time('d.m.Y');?>
			</span>	
		</div>

		<div class="cnt">
			<h2><?php echo the_title();?></h2>
		</div>

		<span class="more"><?php pll_e('Read more');?></span>
	</a>
</li>