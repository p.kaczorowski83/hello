<?php
/**
 * ===============================
 * SUB HERO.PHP - sub lead
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$sustainability_lead_title = get_field( 'sustainability_lead_title' );
$sustainability_lead_cnt = get_field( 'sustainability_lead_cnt' );
?>
<div class="sustainability-lead mt" data-scroll-section="">
	<div class="container">

		<div class="sustainability-lead-row">
			
			<div class="col" data-aos="fade-up">
				<?php if ($sustainability_lead_title): ?>
					<h2 class="typo">
						<?php echo $sustainability_lead_title; ?>
					</h2>
				<?php endif ?>
			</div>

			<div class="col" data-aos="fade-up" data-aos-delay="200">
				<?php echo $sustainability_lead_cnt; ?>
			</div>

		</div>
		
	</div>
</div>