<?php
/**
 * ===============================
 * SUB HERO.PHP - sub download
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$sustainability_download_subtitle = get_field( 'sustainability_download_subtitle' );
$sustainability_download_title = get_field( 'sustainability_download_title' );
?>
<div class="sustainability-download" data-scroll-section="">

	<?php $sustainability_download = get_field( 'sustainability_download' ); ?>
	<?php $size = 'image1920c'; ?>
	<?php if ( $sustainability_download ) : ?>
		<div class="sustainability-download-image">
			<div class="container-2">
				<?php echo wp_get_attachment_image( $sustainability_download, $size, false, [
				'class' => 'lazyload img-fluid',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $sustainability_download , $size ),
				'alt' => get_post_meta( $sustainability_download , '_wp_attachment_image_alt', true),
				]); 
				?>
			</div>
		</div>
	<?php endif; ?>

	<!-- APLA -->
	<div class="sustainability-download-apla">
		<div class="container">
			<?php if ($sustainability_download_subtitle): ?>
				<div class="subtitle" data-aos="fade-up">
					<?php echo $sustainability_download_subtitle ?>
				</div>
			<?php endif ?>
			<?php if ($sustainability_download_title): ?>
				<div class="title" data-aos="fade-up">
					<?php echo $sustainability_download_title; ?>
				</div>
			<?php endif ?>

			<?php if ( get_field( 'sustainability_download_btn' ) ) : ?>
				<div data-aos="fade-up">
					<a href="<?php the_field( 'sustainability_download_btn' ); ?>" class="btn-white"><?php echo pll_e('Download report');?></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>