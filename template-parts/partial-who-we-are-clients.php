<?php
/**
 * ===============================
 * WHO WE ARE.PHP - who we are clients
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$who_we_are_clients_title_1 = get_field('who_we_are_clients_title_1');
$who_we_are_clients_title_2 = get_field('who_we_are_clients_title_2');
?>
<?php if ( have_rows( 'who_we_are_clients_loop' ) ) : ?>
<div class="who-we-are-clients" data-scroll-section="">
	
	<div class="container">
		<h3 class="typo">
			<span data-aos="fade-up"><?php echo $who_we_are_clients_title_1; ?></span>
			<span data-aos="fade-up" data-aos-delay="100"><?php echo $who_we_are_clients_title_2; ?></span>
		</h3>

		<ul>
			<?php while ( have_rows( 'who_we_are_clients_loop' ) ) : the_row(); ?>
				<li data-aos="fade-up">
				<?php $who_we_are_clients_loop_logo = get_sub_field( 'who_we_are_clients_loop_logo' ); ?>
				<?php $size = 'full'; ?>
				<?php if ( $who_we_are_clients_loop_logo ) : ?>
					<?php echo wp_get_attachment_image( $who_we_are_clients_loop_logo, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $who_we_are_clients_loop_logo , $size ),
						'alt' => get_post_meta( $who_we_are_clients_loop_logo , '_wp_attachment_image_alt', true),
						]); 
					?>
				<?php endif; ?>
				</li>
			<?php endwhile; ?>
			</ul>

	</div>
</div>
<?php endif; ?>