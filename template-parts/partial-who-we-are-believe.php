<?php
/**
 * ===============================
 * WHO WE ARE.PHP - who we are believe
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$who_we_are_believe_title = get_field('who_we_are_believe_title');
$who_we_are_believe_cnt = get_field('who_we_are_believe_cnt');

?>
<div class="who-we-are-believe gsap" data-scroll-section="">
	<div class="container-2">
		<div id="image" style="background: url('<?php the_field( 'who_we_are_believe_img' ); ?>');">
		</div>
		<div id="mobile" data-aos="fade-up">
			<img src="<?php the_field( 'who_we_are_believe_img' ); ?>" class="img-fluid">
		</div>
	</div>

	<div class="who-we-are-believe-text full" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>
		
		<?php if ($who_we_are_believe_title): ?>
			<h4 class="typo">
				<?php echo $who_we_are_believe_title; ?>
			</h4>
		<?php endif ?>

		<?php echo $who_we_are_believe_cnt; ?>
	</div>
</div>