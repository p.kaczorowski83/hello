<?php
/**
 * ===============================
 * FOOTER COL .PHP - footer col
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<!-- FOOTER COL -->
<div class="container">

	<div class="footer-col">

		<!-- LOGO -->
		<div class="col logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo-footer.svg" alt="<?php bloginfo('name'); ?>" width="150" height="48">				
			</a>
		</div>

		<!-- MENU -->
		<div class="col">
			<nav aria-label="Footer">
		        <?php
		        wp_nav_menu(
		            array(
		            'theme_location' => 'main-menu',
		            'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
		            'walker'         => new WP_Bootstrap_Navwalker(),
		            )
		        );
		        ?>
		    </nav> 
		</div>

		<!-- SOCIAL -->
		<div class="col social">
			<?php if ( have_rows( 'social','option' ) ) : ?>
				<ul class="social-links social-box">
				<?php while ( have_rows( 'social','option' ) ) : the_row();
				$social_link = get_sub_field('social_link'); ?>
					<li>
						<a href="<?php echo $social_link; ?>" target="_blank" class="item">
							<?php $social_icon = get_sub_field( 'social_icon' ); ?>
							<?php $size = 'full'; ?>
							<?php if ( $social_icon ) : ?>
								<?php echo wp_get_attachment_image( $social_icon, $size, false, [
								'class' => 'lazyload img-fluid',
								'loading' => 'lazy',
								'data-src' => wp_get_attachment_image_url( $social_icon , $size ),
								'alt' => get_post_meta( $social_icon , '_wp_attachment_image_alt', true),
								]); 
								?>
							<?php endif; ?>
						</a>
					</li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>

	</div><!-- edn .footer-col -->