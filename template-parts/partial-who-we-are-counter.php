<?php
/**
 * ===============================
 * WHO WE ARE.PHP - who we are counter
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="who-we-are-counter" data-scroll-section="">
	<div class="container">
		
		<?php if ( have_rows( 'who_we_are_counter' ) ) : ?>
		<ul>
			<?php $i=0; while ( have_rows( 'who_we_are_counter' ) ) : the_row();
			$i++;
			$who_we_are_counter_number = get_sub_field('who_we_are_counter_number');
			$who_we_are_counter_cnt = get_sub_field('who_we_are_counter_cnt');
			?>
			<li>
				<div data-aos="fade-up" data-aos-delay="<?php echo $i ?>00">
					<span>
						<?php echo $who_we_are_counter_number; ?>
					</span>
					<?php echo $who_we_are_counter_cnt; ?>
				</div>
			</li>
			<?php endwhile; ?>
		</ul>
		<?php endif; ?>

	</div>
</div>