<?php
/**
 * ===============================
 * HOME CONTACT.PHP - home contact
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="home-contact" data-scroll-section="">

	<?php if ( have_rows( 'home_contact' ) ) : ?>

		<?php $i = 0; while ( have_rows( 'home_contact' ) ) : the_row(); $i++ ?>
		<?php $home_contact_img = get_sub_field( 'home_contact_img' ); ?>
		<?php $size = 'image1920b'; ?>
		<?php if ( $home_contact_img ) : ?>
			<div class="home-contact-image">
				<?php echo wp_get_attachment_image( $home_contact_img, $size, false, [
					'class' => 'lazyload img-fluid cursor__media',
					'loading' => 'lazy',
					'data-src' => wp_get_attachment_image_url( $home_contact_img , $size ),
					'alt' => get_post_meta( $home_contact_img , '_wp_attachment_image_alt', true),
					]); 
				?>
			</div>
		<?php endif; ?>
		<?php endwhile; ?>

		<ul>
			<?php while ( have_rows( 'home_contact' ) ) : the_row(); ?>
				<li class="">
					<div class="cnt <?php if (!wp_is_mobile()): ?>nav__link<?php endif;?>">
						<div class="subtitle" data-aos="fade-up">
							<?php the_sub_field( 'home_contact_subtitle' ); ?>
						</div>
						<h6 data-aos="fade-up">
							<?php the_sub_field( 'home_contact_title' ); ?>
						</h6>
						
						<?php $home_contact_link = get_sub_field( 'home_contact_link' ); ?>
						<?php if ( $home_contact_link ) : ?>
							<div data-aos="fade-up">
								<a href="<?php echo esc_url( $home_contact_link['url'] ); ?>" class="btn-white"><?php echo esc_html( $home_contact_link['title'] ); ?></a>
							</div>
						<?php endif; ?>
					</div>
				</li>
			<?php endwhile; ?>			
		</ul>

	<?php endif;?>

</div>