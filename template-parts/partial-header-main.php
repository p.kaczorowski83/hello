<?php
/**
 * ===============================
 * HEADER-MAIN.PHP - menu and submenu
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="navbar-main">
    <!-- MENU  -->
    <nav aria-label="Main">
        <?php
        wp_nav_menu(
            array(
            'theme_location' => 'main-menu',
            'container'      => '',
            'menu_class'     => 'navbar-fixed-main-nav',
            'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
            'walker'         => new WP_Bootstrap_Navwalker(),
            )
        );
        ?>
    </nav> 

    <!-- LANG -->
    <div class="navbar-main-lang" style="display: none;">
        <ul>
            <?php pll_the_languages(); ?>
        </ul>
    </div> 
</div>