<?php
/**
 * ===============================
 * HOME.PHP - home work
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$wwd_work_title = get_field('wwd_work_title');
$wwd_work_cnt = get_field( 'wwd_work_cnt' );
?>
<div class="what-we-do-work" data-scroll-section="">
	<div class="container">
		
		<!-- CNT -->
		<div class="what-we-do-work-cnt">
			<h3 data-aos="fade-up">
				<?php echo $wwd_work_title ?>
			</h3>
			<div data-aos="fade-up">
				<?php echo $wwd_work_cnt; ?>
			</div>
			<?php $wwd_work_link = get_field( 'wwd_work_link' ); ?>
			<?php if ( $wwd_work_link ) : ?>
			<div data-aos="fade-up">
				<a href="<?php echo esc_url( $wwd_work_link['url'] ); ?>" class="btn-orange"><?php echo esc_html( $wwd_work_link['title'] ); ?></a>
			</div>
			<?php endif; ?>
		</div>

	</div>

	<?php $wwd_work = get_field( 'wwd_work' ); ?>
	<?php if ( $wwd_work ) : ?>
		<div class="container-2" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>
	<ul <?php if (wp_is_mobile()): ?>class="owl-carousel"<?php else: ?>data-aos="fade-up"<?php endif ?>>
	<?php $i=0; foreach ( $wwd_work as $post ) : ?>
		<?php setup_postdata ( $post ); ?>
		<?php $i++;?>
		<li <?php if (wp_is_mobile()): ?>class="item"<?php endif ?>>
			<a href="<?php the_permalink(); ?>">
				<div class="image">
					<?php $work_img = get_field( 'work_img' ); ?>
					<?php $size = 'image500'; ?>
					<?php if ( $work_img ) : ?>
						<?php echo wp_get_attachment_image( $work_img, $size, false, [
							'class' => 'lazyload img-fluid',
							'loading' => 'lazy',
							'data-src' => wp_get_attachment_image_url( $work_img , $size ),
							'alt' => get_post_meta( $work_img , '_wp_attachment_image_alt', true),
							]); 
						?>
					<?php endif; ?>
				</div>
				
				<div class="text">
					<h4><?php the_title(); ?></h4>
					<?php
					$post_id = get_the_ID(); // Get the current post ID (custom post type)
					$taxonomy_name = 'cat-work'; // Replace with your custom taxonomy slug
					$terms = get_the_terms( $post_id, $taxonomy_name );
					if ( $terms ) {
					$isFirst = true; // Flag to track the first category
					foreach ( $terms as $term ) {
					 $prefix = ($isFirst) ? '' : '. '; // Add a dot and space after the first category
					 echo '<div class="cat">'. $term->name . '</div>';
					 $isFirst = false;
					 }
					}
					?>
				</div>
			</a>
		</li>
	<?php endforeach; ?>
	</ul>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
	</div>
</div>