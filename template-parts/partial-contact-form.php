<?php
/**
 * ===============================
 * CONTACT FORM.PHP - contact form
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$contact_title = get_field('contact_title');
$contact_cnt = get_field('contact_cnt')
?>

<div class="contact-form mt" data-scroll-section="" data-scroll-section-id="section1">
	
	<div class="container-2">

		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/bg-contact.png" alt="">
		
		<div class="container contact-form-row">
			
			<!-- TEXT -->
			<div class="text" data-scroll="" data-scroll-speed="-.3">
				<?php if ($contact_title ): ?>
					<h1 class="typo" data-aos="fade-right">
						<?php echo $contact_title; ?>
					</h1>
				<?php endif ?>	
				<div data-scroll="">
					<div data-aos="fade-right" data-aos-delay="100">
						<?php echo $contact_cnt; ?>
					</div>	
				</div>
			</div>
			<div class="form" data-aos="fade-up">
				<?php echo do_shortcode('[contact-form-7 id="d5ac8ce" title="Contact"] ');?>

				<iframe width="640px" height="480px" src="https://forms.office.com/r/yAmMSmJhSZ?embed=true" frameborder="0" marginwidth="0" marginheight="0" style="border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>
			</div>

		</div>

	</div>

</div>