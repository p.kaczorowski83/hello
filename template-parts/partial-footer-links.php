<?php
/**
 * ===============================
 * FOOTER COL .PHP - footer links
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$sodova = get_field('sodova', 'option');
?>
	<!-- FOOTER COL -->
	<div class="footer-links">

		<!-- LINKS -->
		<div class="links">
			<?php if ( have_rows( 'social_links', 'option' ) ) : ?>
				<ul>
				<?php while ( have_rows( 'social_links', 'option' ) ) : the_row(); ?>
					<?php $social_links_item = get_sub_field( 'social_links_item' ); ?>
					<?php if ( $social_links_item ) : ?>
						<li>
							<a href="<?php echo esc_url( $social_links_item['url'] ); ?>"><?php echo esc_html( $social_links_item['title'] ); ?></a>
						</li>
					<?php endif; ?>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>

		<!-- SODOVA -->
		<div class="sodova">
			<?php echo $sodova; ?>
		</div>

	</div><!-- edn .footer-links -->

</div><!-- end .container -->
