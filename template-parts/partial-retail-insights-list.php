<?php
/**
 * ===============================
 * WORK.PHP - work list
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$work_title = get_field('work_title');
$work_cnt = get_field('work_cnt')
?>
<div class="news-list">

	<div class="container">
		
		<ul class="news-loop" data-aos="fade-up">
		<?php
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$ppp = 9;
		$query = new WP_Query( array(
			'nopaging'               => false,
			'posts_per_page' => $ppp,
			'paged' => $paged,
			'post_type'              => 'retail-insights',
			'post_status'			 => 'publish',
		) ); 
		?>
		<?php if ( $query->have_posts() ) : ?>
        <?php while ( $query->have_posts() ) : $query->the_post();?>
        	<?php get_template_part( 'template-parts/partial', 'retail-insights-list-loop'); ?>
        <?php endwhile;?>
		<?php endif;?>
		<?php wp_reset_postdata(); ?>			  
		</ul>

		<?php if (paginate_links()): ?>
			
		<div class="pagination" data-aos="fade-up">
            <?php 
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => '',
                    'next_text'    => '',
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
            ?>
        </div>
		<?php wp_reset_postdata();
		endif; ?>	

	</div>

</div>