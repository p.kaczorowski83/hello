<?php
/**
 * ===============================
 * NEWS SINGLE HEADER.PHP - news single header
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="news-single-header" data-scroll-section="">
	<div class="container" data-scroll="">
		<h1 data-aos="fade-up">
			<?php echo the_title();?>
		</h1>
		<div class="date" data-aos="fade-up">
			<?php echo the_time('d.m.Y'); ?>
		</div>
	</div>
</div>