<?php
/**
 * ===============================
 * CONTACT TORONTO.PHP - contact toronto
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$contact_toronto_gin_1 = get_field( 'contact_toronto_gin_1' );
$contact_toronto_gin_2 = get_field( 'contact_toronto_gin_2' );
$contact_toronto_subtitle = get_field( 'contact_toronto_subtitle' );
$contact_toronto_title = get_field( 'contact_toronto_title' );
$contact_toronto_text = get_field( 'contact_toronto_text' ); 
?>

<div class="contact-toronto" data-aos="fade-up">
	
	<div class="container-2">
		
		<!-- IMAGE -->
		<?php $contact_toronto_img = get_field( 'contact_toronto_img' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $contact_toronto_img ) : ?>
		<div class="contact-toronto-img">
			<?php echo wp_get_attachment_image( $contact_toronto_img, $size, false, [
				'class' => 'lazyload img-fluid',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $contact_toronto_img , $size ),
				'alt' => get_post_meta( $contact_toronto_img , '_wp_attachment_image_alt', true),
				]); 
			?>	
		</div>
		<?php endif; ?>

		<!-- CNT -->
		<div class="contact-toronto-cnt">

			<!-- GIT -->
			<div class="contact-toronto-cnt-git">
				<span><?php echo $contact_toronto_gin_1;?></span>
				<span><?php echo $contact_toronto_gin_2;?></span>
			</div>

			<div class="container">

				<div class="contact-toronto-cnt-apla">
					<?php if ($contact_toronto_subtitle): ?>
					<div class="subtitle gsap">
						<?php echo $contact_toronto_subtitle; ?>
					</div>	
					<?php endif; ?>
					<?php if ($contact_toronto_title): ?>
					<div class="title gsap">
						<?php echo $contact_toronto_title; ?>
					</div>
					<?php endif ?>
					<?php if ($contact_toronto_text): ?>
					<div class="text gsap">
						<?php echo $contact_toronto_text; ?>	
					</div>
					<?php endif ?>
				</div>
			</div>
		</div>

	</div>

</div>