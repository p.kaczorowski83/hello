<?php
/**
 * ===============================
 * WHO WE ARE.PHP - who we are global
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$who_we_are_global_lead = get_field('who_we_are_global_lead');
$who_we_are_global_cnt = get_field('who_we_are_global_cnt');

?>
<div class="who-we-are-global" data-scroll-section="">
	
		<!-- LEAD -->
		<?php if ($who_we_are_global_lead): ?>
			<div class="who-we-are-global-lead" data-aos="fade-up">
				<div class="container">
					<?php echo $who_we_are_global_lead ?>
				</div>			
			</div>
		<?php endif ?>

		<!-- IMAGE -->
		<?php $who_we_are_global_img = get_field( 'who_we_are_global_img' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $who_we_are_global_img ) : ?>
			<div class="who-we-are-global-image" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>
				<?php echo wp_get_attachment_image( $who_we_are_global_img, $size, false, [
				'class' => 'lazyload img-fluid',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $who_we_are_global_img , $size ),
				'alt' => get_post_meta( $who_we_are_global_img , '_wp_attachment_image_alt', true),
				]); 
				?>
			</div>
		<?php endif; ?>

		<!-- CNT -->
		<?php if ($who_we_are_global_cnt): ?>
			<div class="who-we-are-global-cnt" data-aos="fade-up">
				<div class="container">
					<?php echo $who_we_are_global_cnt ?>
				</div>
			</div>
		<?php endif ?>

		<!-- MAP -->
		<?php $who_we_are_global_map = get_field( 'who_we_are_global_map' ); ?>
		<?php $who_we_are_global_map_city = get_field( 'who_we_are_global_map_city' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $who_we_are_global_map ) : ?>
		<div class="who-we-are-global-map" data-scroll="" data-scroll-speed=".25">
			<?php echo wp_get_attachment_image( $who_we_are_global_map, $size, false, [
				'class' => 'lazyload img-fluid',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $who_we_are_global_map , $size ),
				'alt' => get_post_meta( $who_we_are_global_map , '_wp_attachment_image_alt', true),
				]); 
			?>

			<?php echo wp_get_attachment_image( $who_we_are_global_map_city, $size, false, [
				'class' => 'lazyload img-fluid city',
				'loading' => 'lazy',
				'data-src' => wp_get_attachment_image_url( $who_we_are_global_map_city , $size ),
				'alt' => get_post_meta( $who_we_are_global_map_city, '_wp_attachment_image_alt', true),
				]); 
			?>
		</div>			
		<?php endif; ?>

	</div>
</div>