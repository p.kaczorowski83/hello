<?php
/**
 * ===============================
 * CAREER COMMUNITY.PHP - career community section
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$career_community_title = get_field('career_community_title');
$career_community_cnt = get_field('career_community_cnt');
?>
<div class="career-community gsap" data-scroll-section="" <?php if (wp_is_mobile()): ?>data-aos="fade-up"<?php endif;?>>

	<div class="container-2">
		<div id="image" style="background: url('<?php the_field( 'career_community_img' ); ?>');"></div>
		<div id="mobile">
			<img src="<?php the_field( 'career_community_img' ); ?>" class="img-fluid">
		</div>
	</div>

	<div class="career-community-text full-right">		
		<?php if ($career_community_title): ?>
			<h4 class="typo">
				<?php echo $career_community_title; ?>
			</h4>
		<?php endif ?>
		<?php echo $career_community_cnt; ?>
	</div>

</div>