<?php
/**
 * ===============================
 * SINGLE RI.PHP - single retail list
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<?php if ( have_rows( 'ri_boxes' ) ) : ?>
<div class="retail-insights-single-list" data-scroll="">
	<ul class="news-loop" data-aos="fade-up">
		<?php while ( have_rows( 'ri_boxes' ) ) : the_row();
		$ri_boxes_title = get_sub_field('ri_boxes_title');
		$ri_boxes_link = get_sub_field('ri_boxes_link');
		?>
			<li>
			<a href="<?php echo $ri_boxes_link?>" target="_blank">
				<div class="image">
					<?php $ri_boxes_img = get_sub_field( 'ri_boxes_img' ); ?>
					<?php $size = 'image400'; ?>
					<?php if ( $ri_boxes_img ) : ?>
						<?php echo wp_get_attachment_image( $ri_boxes_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $ri_boxes_img , $size ),
						'alt' => get_post_meta( $ri_boxes_img , '_wp_attachment_image_alt', true),
						]); 
						?>
					<?php endif; ?>
				</div>

				<div class="cnt">
					<h2><?php echo $ri_boxes_title; ?></h2>
				</div>

				<span class="more"><?php pll_e('Read more');?></span>
			</a>
</li>
		<?php endwhile; ?>
	</ul>
</div>
<?php endif; ?>