<?php
/**
 * ===============================
 * HEADER.PHP - main header file
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$hero_img = get_field( 'hero_img' );
$hero_mp4 = get_field('hero_mp4');
?>

<?php if ( !is_404() ): ?>
<header class="navbar <?php if ($hero_img or $hero_mp4 or is_singular( 'post' ) or is_singular( 'retail-insights' ) or is_post_type_archive('retail-insights') ) : ?>navbar-transparent<?php endif;?> <?php if ($hero_img or $hero_mp4 or is_post_type_archive('retail-insights')  ) :?>navbar-white<?php endif ?>">	
	<div class="navbar-cnt">
		<!-- LOGO -->
		<figure class="navbar-cnt-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" width="127" height="40">				
			</a>		
		</figure>

		<!-- MENU -->
		<?php 
			get_template_part( 'template-parts/partial', 'header-main' ); 
		?>

		<div class="hamburger">
			<svg class="ham hamRotate ham1" viewBox="0 0 100 100" width="80">
				<path class="line top" d="m 30,33 h 40 c 0,0 9.044436,-0.654587 9.044436,-8.508902 0,-7.854315 -8.024349,-11.958003 -14.89975,-10.85914 -6.875401,1.098863 -13.637059,4.171617 -13.637059,16.368042 v 40" />
				<path class="line middle" d="m 30,50 h 40" />
				<path class="line bottom" d="m 30,67 h 40 c 12.796276,0 15.357889,-11.717785 15.357889,-26.851538 0,-15.133752 -4.786586,-27.274118 -16.667516,-27.274118 -11.88093,0 -18.499247,6.994427 -18.435284,17.125656 l 0.252538,40" />
			</svg>
		</div>
	
	</div><!-- end .navbar__fixed-cnt -->
</header>


<?php endif;?>