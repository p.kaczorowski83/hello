<?php
/**
 * ===============================
 * SUB HERO.PHP - sub download
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$sustainability_standards_title = get_field('sustainability_standards_title')
?>
<div class="sustainability-standards" data-scroll-section="">
	<div class="container">
	
		<div class="sustainability-cards-row">

			<div class="tabs" data-aos="fade-up">
				<div class="sticky">
					<h4>
						<?php echo $sustainability_standards_title ?>
					</h4>

					<ul>
					<?php while ( have_rows( 'sustainability_standards' ) ) : the_row();
					$sustainability_standards_title = get_sub_field('sustainability_standards_title') ?>
					<li class="tabs-item">
						<a href="#<?php echo sanitize_title($sustainability_standards_title) ?>">
							<?php echo $sustainability_standards_title ?>
						</a>
					</li>
					<?php endwhile;?>
					</ul>

				</div>
			</div>
			
			<?php if ( have_rows( 'sustainability_standards' ) ) : ?>
			<div class="cards" data-aos="fade-up" data-aos-delay="100">
				<ul>
				<?php while ( have_rows( 'sustainability_standards' ) ) : the_row();
				$sustainability_standards_title = get_sub_field('sustainability_standards_title');
				$sustainability_standards_cnt = get_sub_field('sustainability_standards_cnt');
				$sustainability_standards_link = get_sub_field('sustainability_standards_link')
				?>
				<li id="<?php echo sanitize_title($sustainability_standards_title) ?>">					
					<?php $sustainability_standards_foto = get_sub_field( 'sustainability_standards_foto' ); ?>
					<?php $size = 'image688'; ?>
					<?php if ( $sustainability_standards_foto ) : ?>
						<?php echo wp_get_attachment_image( $sustainability_standards_foto, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $sustainability_standards_foto , $size ),
						'alt' => get_post_meta( $sustainability_standards_foto , '_wp_attachment_image_alt', true),
						]); 
						?>
					<?php endif; ?>

					<div class="apla">
						<div class="title">
							<?php echo $sustainability_standards_title; ?>
						</div>
						<?php echo $sustainability_standards_cnt; ?>
						<?php if ( $sustainability_standards_link ) : ?>
							<a href="<?php echo esc_url( $sustainability_standards_link['url'] ); ?>" class="<?php if (wp_is_mobile()): ?>btn-orange<?php else: ?>btn-white<?php endif ?>"><?php echo esc_html( $sustainability_standards_link['title'] ); ?></a>
						<?php endif; ?>
					</div>
				</li>
				<?php endwhile;?>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>