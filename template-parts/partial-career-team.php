	<?php
/**
 * ===============================
 * CAREER TEAM.PHP - team section
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$career_team_title = get_field( 'career_team_title' );
$career_team_lead = get_field( 'career_team_lead' );
$career_team_cnt = get_field( 'career_team_cnt' )
?>
<div class="career-team" data-scroll-section="">
	<div class="container">
		
		<?php if ($career_team_title): ?>
			<div class="subtitle" data-aos="fade-up">
				<?php echo $career_team_title; ?>
			</div>
		<?php endif ?>
		<?php if ($career_team_lead): ?>
			<h2 data-aos="fade-up" data-aos-delay="100">
				<?php echo $career_team_lead; ?>
			</h2>
		<?php endif ?>

		<?php if ($career_team_cnt): ?>
			<div data-aos="fade-up" data-aos-offset="200">
				<?php echo $career_team_cnt; ?>
			</div>
		<?php endif ?>

	</div>
</div>