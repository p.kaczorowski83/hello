<?php
/**
 * ===============================
 * SUB HERO.PHP - sub hero
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$hero_slogan_1 = get_field( 'hero_slogan_1' );
$hero_slogan_2 = get_field( 'hero_slogan_2' );
$hero_mp4 = get_field('hero_mp4');
$hero_mp4_mobile = get_field('hero_mp4_mobile');
?>
<div class="hero hero-sustainability" data-scroll-section="">


	<?php if ($hero_mp4_mobile): ?>

		<?php if (wp_is_mobile()): ?>
			<video playsinline autoplay muted loop id="video">
				<source src="<?php echo $hero_mp4_mobile ?>" type="video/mp4">
			</video>
		<?php else: ?>
			<video playsinline autoplay muted loop id="video">
				<source src="<?php echo $hero_mp4 ?>" type="video/mp4">
			</video>
		<?php endif ?>
	
	<?php else: ?>
		<?php if ($hero_mp4): ?>
		<video playsinline autoplay muted loop id="video">
			<source src="<?php echo $hero_mp4 ?>" type="video/mp4">
		</video>
		<?php endif ?>
	<?php endif ?>


	<div class="hero-slogan">
		<div class="container">
			<h1 data-scroll="" data-scroll-speed="-.25">
				<span data-aos="fade-up"><?php echo $hero_slogan_1; ?></span>
				<span data-aos="fade-up" data-aos-delay="200"><?php echo $hero_slogan_2; ?></span>
			</h1>
		</div>
	</div>
</div>