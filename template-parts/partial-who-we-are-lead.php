<?php
/**
 * ===============================
 * WHO WE ARE.PHP - who we are lead
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$who_we_are_lead_title = get_field('who_we_are_lead_title');
$who_we_are_lead_cnt = get_field('who_we_are_lead_cnt');

?>
<div class="who-we-are-lead mt" data-scroll-section="">
	<div class="container">
		
		<!-- TITLE -->
		<div class="title" data-aos="fade-up">
			<?php echo $who_we_are_lead_title; ?>
		</div>

		<!-- CNT -->
		<div class="cnt">
			<h2 data-aos="fade-up" data-aos-delay="100">
				<?php echo $who_we_are_lead_cnt; ?>
			</h2>
		</div>

	</div>
</div>