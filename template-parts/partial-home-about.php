<?php
/**
 * ===============================
 * HOME ABOUT.PHP - home about
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
$home_about_subtitle = get_field('home_about_subtitle');
$home_about_title = get_field('home_about_title');
$home_about_cnt = get_field('home_about_cnt')

?>
<div class="home-about" data-scroll-section="">

	<div class="container">
		
		<!-- LEAD -->
		<div class="home-about-lead">
			<?php if ($home_about_subtitle): ?>
				<div data-aos="fade-up">
					<?php echo $home_about_subtitle ?>
				</div>
			<?php endif ?>

			<?php if ($home_about_title): ?>
				<h5 data-aos="fade-up">
					<?php echo $home_about_title ?>
				</h5>
			<?php endif ?>
		</div>

	</div>

	<?php $home_about_img = get_field( 'home_about_img' ); ?>
	<?php $size = 'image1920a'; ?>
	<?php if ( $home_about_img ) : ?>
	<!-- IMAGE -->
	<div class="home-about-image" data-aos="fade-up">
		<?php echo wp_get_attachment_image( $home_about_img, $size, false, [
			'class' => 'lazyload img-fluid',
			'loading' => 'lazy',
			'data-src' => wp_get_attachment_image_url( $home_about_img , $size ),
			'alt' => get_post_meta( $home_about_img , '_wp_attachment_image_alt', true),
			]); 
		?>

		<div class="home-about-image-apla">
			<div class="container">
				<?php the_field( 'home_about_cnt' ); ?>
				<?php $home_about_link = get_field( 'home_about_link' ); ?>
				<?php if ( $home_about_link ) : ?>
					<a href="<?php echo esc_url( $home_about_link['url'] ); ?>" class="btn-white"><?php echo esc_html( $home_about_link['title'] ); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endif;?>

</div>