<?php
/**
 * ===============================
 * ARCHIVE RETIAL INSIGHTS
 * ===============================
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="news" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'retial-insights-hero');
            get_template_part( 'template-parts/partial', 'retail-insights-list');
        ?>
    </main>

<?php
get_footer();