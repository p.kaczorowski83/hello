<?php
/**
 * ===============================
 * TEMPLATE-PAGE-WHAT-WE-DO
 * ===============================
 *
 * Template name: What We Do
 *
 * @package ARRAY
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>
    <main class="what-we-do" data-scroll-container>
        <?php 
            get_template_part( 'template-parts/partial', 'what-we-do-hero');
            get_template_part( 'template-parts/partial', 'what-we-do-boxes');
            get_template_part( 'template-parts/partial', 'what-we-do-work');
            get_template_part( 'template-parts/partial', 'who-we-are-clients');
        ?>
    </main>

<?php
get_footer();